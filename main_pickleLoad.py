from UI_state_machine import *

import pickle
import pandas as pd
import time
import os
from sentence_transformers import SentenceTransformer

sentBERT = SentenceTransformer('roberta-large-nli-stsb-mean-tokens')

########### MAIN()

dbStates = {}
dbPaths = {}
dbPIDs = {}
dbUQs = {}
dbResps = {}

# for SMS: intermediate needs to be different from fullStepResponse
# but in some cases like web chat page they can be the same output func
def intermedRespFunc(bulkMsg, uID):
    #print('##### SENDING One-WAY TO USER:')
    print(bulkMsg)  
    
def logFunc(txt):
    print('\n!% DEBUG messages: ', txt)
    print('\n')

uID = '+14168466302' # from SMS inboud attributes
while True:
    print("User input: ")
    uInp = input()
    uInp = uInp.strip()
        
    # RELOAD DATA in case it changed
    dfM = pickle.load(open("dfM.pickle","rb"))
    dfCq = pickle.load(open("dfCq.pickle","rb"))
    
    run_UI_state_machine(uID, uInp, dfM, dfCq, dbPaths, dbUQs, dbResps, dbStates, dbPIDs, nlp, sentBERT, intermedRespFunc, logFunc) 
    fullStepResponse = dbResps[uID]
    dbResps[uID] = ''
    
    print(fullStepResponse)

    logFunc((uID, 'Ending with state:', dbStates[uID], dbPIDs[uID], 'Final uQ:',dbUQs[uID]))