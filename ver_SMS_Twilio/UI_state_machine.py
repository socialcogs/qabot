###### UI_state_machine.py

from IPython.display import clear_output
from branching import *
import re

def run_UI_state_machine(uID, uInp,\
                         dfM, dfCq, \
                         dbPaths, dbUQs, dbResps, dbStates, dbPIDs, 
                         sentBERT, intermedRespFunc, logFunc):
    
    if uID not in dbStates:
        dbStates[uID] = 'First_Time_User'
        logFunc(('New user: ', uID))
        intermedRespFunc('Hello [NameFromPhonebook],\n(FYI, I am running language algorithms with millions of parameters on a small test machine - Replies could be up to 7 SEC. per message)', uID)

        
    if (dbStates[uID] == 'Answer_Given') and (uInp.isdigit()):
        if int(uInp) >= len(dbPaths[uID]):
            dbResps[uID] = dbResps[uID] + 'Non-existent step number - Please reply with a number from 1 to ' + str(dbPIDs[uID]) + '\n'
        elif int(uInp) == 0:
            dbResps[uID] = dbResps[uID] + '>> A human joins the chat at this time in the prod version (you can continue testing me here with new questions).\n'
        else: # valid digit:
            dbPIDs[uID] = int(uInp) - 1
            node = dfM[dfM.id == dbPaths[uID][dbPIDs[uID]]]       
            children = node['Children'].iloc[0]
            
            dbResps[uID] = dbResps[uID] + \
            'Here are my options at this step:' + '\n'
            
            for optN in range(1, len(children)+1):
                chID = children[optN-1]
                childNode = dfM[dfM.id == chID]
                txt2print = childNode['Txt2User'].iloc[0]
                txt2print = txt2print.partition('\n')[0]
                
                dbResps[uID] = dbResps[uID] + \
                chr(96+optN) + ' - ' + txt2print + '\n'
                
            dbResps[uID] = dbResps[uID] + 'z - None of these apply to me, connect me to someone now\n'
            dbResps[uID] = dbResps[uID] + 'Reply with a single letter please,\nOR you can send me a new question at any time.'
            
            dbStates[uID] = 'Backtracking_Options_Sent'
        
        
    elif (len(uInp) == 1) \
    and (dbStates[uID] == 'Backtracking_Options_Sent' \
         or dbStates[uID] == 'Manual_Branching_Options_Sent'):
        node = dfM[dfM.id == dbPaths[uID][dbPIDs[uID]]]       
        children = node['Children'].iloc[0]
        
        if uInp.isdigit():
            dbResps[uID] = dbResps[uID] + 'You entered a number but I need either a single character for the options above, or you can ask me a new questions.\n'
        elif uInp =='z':
            dbResps[uID] = dbResps[uID] + '>> A human joins the chat at this time in the prod version (you can continue testing me here with new questions).\n'
        # must not be z below 
        elif (ord(uInp.lower()) - ord('a')) >= len(children):
            dbResps[uID] = dbResps[uID] + 'You entered a character outside the possible options below. Please try again OR you can ask me a new questions.\n'
        else: # valid non-z single character                     
            intermedRespFunc('... Computing... should be less than 5 seconds...', uID)            
            choiceLetter = uInp
            choiceLetter = choiceLetter.lower()
            choiceIndex = ord(choiceLetter) - ord('a')
            choiceChildID = children[choiceIndex]
            
            dbPIDs[uID] = dbPIDs[uID] + 1
            dbPaths[uID][dbPIDs[uID]] = choiceChildID
            dbResps[uID] =  'Thank you. I now understand your as follows:' + '\n'
            dbStates[uID] == 'User_Manual_Path_Choice'
            processPathNode(uID, dfM, dfCq, \
                            dbPaths, dbUQs, dbResps, dbStates, dbPIDs, \
                            sentBERT, logFunc)    
    
    
    elif (len(uInp) == 1) and (dbStates[uID] == 'CEQ_Options_Sent'): 
        nInx = dfM[dfM.id == dbPaths[uID][dbPIDs[uID]]].index[0]
        ceqID = dfM.at[nInx, 'ContextExpQ'][0]
        node2 = dfCq[dfCq.id == ceqID]
        
        if uInp.isdigit():
            dbResps[uID] = dbResps[uID] + 'You entered a number but I need either a single character for the options above, or you can ask me a new questions.\n'
        elif uInp =='z':
            dbResps[uID] = dbResps[uID] + '>> A human joins the chat at this time in the prod version (you can continue testing me here with new questions).\n'
        # must not be z below 
        elif (ord(uInp.lower()) - ord('a')) \
        >= int(node2['Number Of Options'].iloc[0]):
            dbResps[uID] = dbResps[uID] + 'You entered a character outside the possible options below. Please try again OR you can ask me a new questions.\n'
        else: # valid non-z single character           
            intermedRespFunc('... Computing... should be less than 5 seconds...', uID)
            
            choiceLetter = uInp
            choiceLetter = choiceLetter.lower()
            choiceNum = ord(choiceLetter) - ord('a') + 1
            colName = '#' + str(choiceNum) + ' Addition to Context'
            
            dbUQs[uID] = dbUQs[uID] + ', ' + node2[colName].iloc[0]

            dbResps[uID] = \
            'Thank you, so I now understand the following:' + '\n'
            dbStates[uID] = 'CEQ_Answered'
            branchPath(uID, dfM, dfCq, \
                       dbPaths, dbUQs, dbResps, dbStates, dbPIDs,\
                       sentBERT, logFunc)
    
    
    else: # a new question/text, path reset from 0
        # REMOVE IN SMS & WEB versions
        #clear_output(wait=True)
        defaultID = dfM[dfM.AlgTxt=='<Default>']['id'].iloc[0]        
        dbUQs[uID] = uInp 
        dbPIDs[uID] = 0
        dbPaths[uID] = {}
        
        if dbStates[uID] == 'First_Time_User':
            intermedRespFunc('... Computing... If I make a mistake below just enter the numbers you will see (ex: 1, 2, 3, etc.) to correct me.\n', uID)
        else:
            intermedRespFunc('Trying to understand your new question... should be less than 5 seconds.\n', uID)     
        
        dbResps[uID] = 'I think your question:\n'
        # Default starting point unless revised by an NER-Jump below
        # plus needed if backtracking from an NER answer - to start from top
        dbPaths[uID][0] = defaultID
        
        # Check NER-Jumps
        matchedNERs = []
        txt2User = ''
        for index, row in dfM.iterrows():
            if type(row['NER Jump']) is str:
                regexRes =re.findall(row['NER Jump'], \
                                     dbUQs[uID], re.IGNORECASE)
                if len(regexRes) > 0:
                    matchedNERs.append(row['id'])
                    txt2User = row['Txt2User']
        logFunc(('uID', uID, 'NER Matched: ', matchedNERs))
        if len(matchedNERs) > 0: # NER jumping
            dbPIDs[uID] = 1
            dbPaths[uID][1] = matchedNERs[0]
            if not nodeIsLeaf(dbPaths[uID][1], dfM):  
                dbResps[uID] = dbResps[uID] + '1: ' + txt2User + '\n'
        
        branchPath(uID, dfM, dfCq, \
                   dbPaths, dbUQs, dbResps, dbStates, dbPIDs,\
                   sentBERT, logFunc)
    ""