##### branching.py

import numpy as np
import pandas as pd
import math
import re
import scipy.spatial
import sys

def softmax(x):
    return np.exp(x) / np.sum(np.exp(x), axis=0) 
   
def getWinningBranch(uID, nodesIDs, uQ, sentBERT, dfM, logFunc):
    # runs non-smart even with a single branch
    uQemb = sentBERT.encode([uQ])    
    invCosDists = []
    algTxts = []
    for nID in nodesIDs:
        nInx = dfM[dfM.id == nID].index[0]
        nodeAns = dfM.at[nInx, 'AlgTxt']
        algTxts.append(nodeAns)
        subTexts = nodeAns.split(';')
        bestSubTxtDist = 1
        for subTxt in subTexts:
            subTxtEmb = sentBERT.encode([subTxt])
            thisDist = scipy.spatial.distance.cdist(uQemb, subTxtEmb,\
                                                   "cosine")[0]
            if thisDist[0] < bestSubTxtDist:
                bestSubTxtDist = thisDist[0]
        
        invCosDists.append(1 - bestSubTxtDist)
    # It's INVERTED (1-x) cosDist
    decShift = np.array(invCosDists) * 10
    probs = softmax(decShift)
    maxIndex = probs.argmax()
    
    allProbLog = '{\nuID ' + uID + ':\n'
    allProbLog = allProbLog + 'NLU probabilities for \"' + uQ + '\":\n'
    
    sortOrd = probs.argsort()
    for i in sortOrd:
        allProbLog = allProbLog + '%.4f' %probs[int(i)] + ' ' \
        + algTxts[int(i)] + '\n'
    allProbLog = allProbLog + '}'
    logFunc(allProbLog)

    return [nodesIDs[maxIndex], probs[maxIndex]]


def nodeIsLeaf(nID, dfM):
    node = dfM[dfM.id == nID]        
    children = node['Children'].iloc[0]
    if type(children) is list:
        return False
    else:
        return True

    
def processPathNode(uID, dfM, dfCq, \
                    dbPaths, dbUQs, dbResps, dbStates, dbPIDs, \
                    sentBERT, logFunc):

    node = dfM[dfM.id == dbPaths[uID][dbPIDs[uID]]]

    if nodeIsLeaf(dbPaths[uID][dbPIDs[uID]], dfM):

        dbResps[uID] = dbResps[uID] + str(dbPIDs[uID]) + \
        ': This is the best answer I found:\n'\
        + '##################\n'

        dbResps[uID] = dbResps[uID] + node['Txt2User'].iloc[0] + '\n'
        dbResps[uID] = dbResps[uID] + '##################\n'

        dbResps[uID] = dbResps[uID] + '\nReply with the number ' + str(dbPIDs[uID]) + ' if you want to browse alternative answers\n'
        
        if dbPIDs[uID] == 2:
            dbResps[uID] = dbResps[uID] + 'Reply with the number 1 to revise my assumption above\n'
        elif dbPIDs[uID] > 2:
            dbResps[uID] = dbResps[uID] + 'Reply with a number from 1 to ' + str(dbPIDs[uID]-1) + ' to revise my assumptions above\n'
        
        dbResps[uID] = dbResps[uID] + 'Reply with the number 0 if you want to be transferred.\n' + 'OR you can ask me a new question at any time.\n'

        dbStates[uID] = 'Answer_Given'
    else:
        dbResps[uID] = dbResps[uID] + str(dbPIDs[uID]) + ": " \
                      + node['Txt2User'].iloc[0] + '\n'
        branchPath(uID, dfM, dfCq, \
                   dbPaths, dbUQs, dbResps, dbStates, dbPIDs, \
                   sentBERT, logFunc)

        
def branchPath(uID, dfM, dfCq, \
               dbPaths, dbUQs, dbResps, dbStates, dbPIDs, \
              sentBERT, logFunc):
    nInx = dfM[dfM.id == dbPaths[uID][dbPIDs[uID]]].index[0]
    
    if nodeIsLeaf(dbPaths[uID][dbPIDs[uID]], dfM):
        processPathNode(uID, dfM, dfCq, \
                        dbPaths, dbUQs, dbResps, dbStates, dbPIDs, \
                        sentBERT, logFunc)
    else:   
        readyForBranching = True

        if (type(dfM.at[nInx, 'ContextExpQ']) is list):            
            ceqID = dfM.at[nInx, 'ContextExpQ'][0]
            ceqInx = dfCq[dfCq.id == ceqID].index[0]

            # REGEX skipping of all below is a must in this version
            regexRes = re.findall(dfCq.at[ceqInx, \
                                          'Skip If This RegEx Matches'],\
                                  dbUQs[uID], re.IGNORECASE)
            if len(regexRes) == 0: 
                dbResps[uID] = dbResps[uID] + '\n'
                dbResps[uID] = dbResps[uID] + \
                'I now need to clarify the following please:' + '\n'

                dbResps[uID] = dbResps[uID] + dfCq.at[ceqInx, 'Txt'] + '\n'

                dbResps[uID] = dbResps[uID] +\
                'Reply with a single letter please:' + '\n'

                node2 = dfCq[dfCq.id == ceqID]
                for optN in \
                range(1, int(node2['Number Of Options'].iloc[0])+1):
                    colName = '#' + str(optN) + ' Prompt To User'

                    dbResps[uID] = dbResps[uID] \
                    + chr(96+optN) + ' - ' + node2[colName].iloc[0] + '\n'

                dbResps[uID] = dbResps[uID] + 'z - None of these apply to me, transfer me to someone now\n'
                dbResps[uID] = dbResps[uID] + 'You can also send me a new question.'
                
                dbStates[uID] = 'CEQ_Options_Sent'
                readyForBranching = False

        if readyForBranching == True:
            node = dfM[dfM.id == dbPaths[uID][dbPIDs[uID]]]         
            children = node['Children'].iloc[0]

            # Only SentBERT now, but later others too & voting
            getWinner = getWinningBranch(uID, children, dbUQs[uID], \
                                         sentBERT, dfM, logFunc)
            winID = getWinner[0]
            winProb = getWinner[1]
            
            alwaysAutoBranch = False
            if (winProb > 0.60) or alwaysAutoBranch: # Confident Enough
                dbPIDs[uID] += 1
                dbPaths[uID][dbPIDs[uID]] = winID
                processPathNode(uID, dfM, dfCq, \
                                dbPaths, dbUQs, dbResps, dbStates, dbPIDs,
                                sentBERT, logFunc)
            else: # Not confident for this branch
                dbResps[uID] = dbResps[uID] + \
                'Sorry, I am only ' + str(int(winProb * 100)) + '% confident next, so I would like to verify with you which of these best applies to your question:\n'
                for optN in range(1, len(children)+1):
                    chID = children[optN-1]
                    childNode = dfM[dfM.id == chID]
                    txt2print = childNode['Txt2User'].iloc[0]
                    txt2print = txt2print.partition('\n')[0]

                    dbResps[uID] = dbResps[uID] + \
                    chr(96+optN) + ' - ' + txt2print + '\n'
                
                dbResps[uID] = dbResps[uID] + 'z - None of these apply to me, connect me to someone now\n'
                dbResps[uID] = dbResps[uID] + 'Reply with a single letter please,\nOR you can send me a new question at any time.'
                
                dbStates[uID] = 'Manual_Branching_Options_Sent'

