# /usr/bin/env python
# Download the twilio-python library from twilio.com/docs/libraries/python
from flask import Flask, request
from twilio.twiml.messaging_response import MessagingResponse
from twilio.rest import Client

from UI_state_machine import *
from load_and_fix_data import *

import time
import os
from sentence_transformers import SentenceTransformer

from IPython.core.display import display, HTML

from twilio.rest import Client
account_sid = os.environ['TWILIO_ACCOUNT_SID']
auth_token = os.environ['TWILIO_AUTH_TOKEN']
client = Client(account_sid, auth_token)

sentBERT = SentenceTransformer('roberta-large-nli-stsb-mean-tokens')

dbStates = {}
dbPaths = {}
dbPIDs = {}
dbUQs = {}
dbResps = {}

def intermedRespFunc(bulkMsg, uID):
    client.messages \
    .create(
        body=bulkMsg,
        messaging_service_sid='MG6c9ef8c89b5ddb6f1946b066617bca38',
        to=uID
    )

def logFunc(txt):
    print('\n!% DEBUG messages: ', txt)
    print('\n')


app = Flask(__name__)

@app.route("/sms", methods=['GET', 'POST'])
def sms_ahoy_reply():
    """Respond to incoming messages with a friendly SMS."""
    uID = request.values.get('From', None)
    uInp = request.values.get('Body', None)
    uInp = uInp.strip()
    
    # RELOAD DATA in case it changed
    dfM = load_dfM()
    logTxt = auto_complete_links(dfM)
    logFunc(logTxt)
    dfCq = load_dfCq()
    
    # Start our response
    resp = MessagingResponse()
    
    run_UI_state_machine(uID, uInp, dfM, dfCq, dbPaths, dbUQs, dbResps, dbStates, dbPIDs, sentBERT, intermedRespFunc, logFunc) 
    fullStepResponse = dbResps[uID]
    dbResps[uID] = ''
    
    if dbStates[uID] == 'Answer_Given':
        answTitleOnly = fullStepResponse.partition('##################')[2].partition('\n')[2].partition('\n')[0]
        logFunc('{\nuID ' + uID + ':\nSENDING MESSAGE TO USER:\n' \
                + answTitleOnly + '}\n')
    else:
        logFunc('{\nuID ' + uID + ':\nSENDING MESSAGE TO USER:\n' \
                + fullStepResponse + '}\n')
    
    resp.message(fullStepResponse)

    logFunc(('Ending with state:', dbStates[uID], dbPIDs[uID]))

    return str(resp)

if __name__ == "__main__":
    app.run(debug=True)
