from UI_state_machine import *
from bot_calls import ask1
import pickle
from sentence_transformers import SentenceTransformer, CrossEncoder

class NLP:
    pass

nlp = NLP()

models_path = /home/martindkaz/Dropbox/NLP_resources/nlp_models/

# nlp.lang = 'FR'
nlp.lang = 'EN'
# nlp.type = 'siamese'
nlp.type = 'cross_encoder'
# nlp.model = CrossEncoder('sentence-transformers/ce-roberta-large-stsb')
nlp.model = CrossEncoder(models_path + 'ceLarge_02')

# nlp.model = SentenceTransformer('distilbert-multilingual-nli-stsb-quora-ranking')
# nlp.model = SentenceTransformer('distilroberta-base-paraphrase-v1')

# nlp.model = SentenceTransformer(models_path + 'sentBERT-allGood_wSingeltons')
# nlp.model = SentenceTransformer(models_path + 'sentBERT2')

folder = ''
# folder = 'tests/'

eval_bot = pickle.load(open(folder + "pickle.eval_bot_" + nlp.lang,"rb"))
failQs = []

incorrect_answers = 0
counting_qs = 0
for [q, node] in eval_bot:
    counting_qs = counting_qs + 1
    
    resolved_node = ask1(nlp, q)
    if node == resolved_node:
        print('\n############### Correct\n')
    else:
        print('\n############### Incorrect\n')
        incorrect_answers = incorrect_answers + 1
        failQs.append((q, resolved_node))
    
print('### FAILED QUESTIONS')
for q in failQs:
    print(q)
print('Accuracy: ', 1.0 - (incorrect_answers/counting_qs))