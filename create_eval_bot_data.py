import pickle

lang = 'FR'

# Questions different from AlgTxt subTxts
if lang == 'EN':
    eval_bot = []
else:
    eval_bot = []

folder = ''
# folder = 'tests/'

dfM = pickle.load(open(folder + "pickle.HD_kdb","rb"))
for index, row in dfM.iterrows():
    # Skip special cases without AlgTxt, like Root or TransferNow
    if row[lang + '_AlgTxt'] == '': continue
    # Want only leaves:
    if type(row['Children']) != float: continue
    # Skip off-Root leaves since a sure match uQ-to-subTxt:
    if row['Parents'] == ['ROOT']: continue
    # skips special cases like Not_Yet_In_KDB
    if row['id']== 'NOT YET IN KDB': continue

    subTxts = row[lang + '_AlgTxt'].split(';')
    for sT in subTxts:
        eval_bot.append([sT, row['id']])

print(eval_bot)

pickle.dump(eval_bot, open(folder + "pickle.eval_bot_" + lang, "wb"))
