from UI_state_machine import *
from sentence_transformers import SentenceTransformer
import pickle
import re

folder = ''
# folder = 'tests/'

def ask1(nlp, q):
    dfM = pickle.load(open(folder + "pickle.HD_kdb","rb"))
    dfCq = 'dummy - not used - function needs it as arg'

    def logFunc(txt):
        print('\n!% DEBUG messages: ', txt)
        print('\n')

    dbStates = {}
    dbPaths = {}
    dbPIDs = {}
    dbUQs = {}
    dbResps = {}
    uID = 'user1'

    dbUQs[uID] = q.lower()
    print(('Asking question', dbUQs[uID]))
    dbPaths[uID] = {}
    dbPaths[uID][0] = 'ROOT'
    dbResps[uID] = ''
    dbPIDs[uID] = 0
    
    # Check NER-Jumps
    matchedNERs = []
    txt2User = ''
    for index2, row2 in dfM.iterrows():
        if type(row2['NER Jump']) is str and row2['NER Jump'] != '':
            regexRes =re.findall(row2['NER Jump'], \
                                dbUQs[uID], re.IGNORECASE)
            if len(regexRes) > 0:
                matchedNERs.append(row2['id'])
                txt2User = "txt2user"
    if len(matchedNERs) > 0: 
        print('NER matched: ', matchedNERs)
        dbPIDs[uID] = 1
        dbPaths[uID][1] = matchedNERs[0]
        if not nodeIsLeaf(dbPaths[uID][1], dfM):  
            dbResps[uID] = dbResps[uID] + '1: ' + txt2User + '\n'
    
    branchPath(uID, dfM, dfCq, \
                dbPaths, dbUQs,   dbResps, dbStates, dbPIDs, nlp,  logFunc)
    print(dbResps[uID])
    dbResps[uID] = ''
    
    resolved_node = dbPaths[uID][len(dbPaths[uID]) - 1]
    print(resolved_node)
    return resolved_node
