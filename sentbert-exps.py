import pickle
import numpy as np
import pandas as pd
import scipy.spatial
from sentence_transformers import SentenceTransformer, CrossEncoder, InputExample, losses
from torch.utils.data import DataLoader
from bot_calls import ask1
from sentbert_calls import *

class NLP:
    pass

models_path = /home/martindkaz/Dropbox/NLP_resources/nlp_models/

en2 = NLP()
en2.lang = 'EN'
en2.type = 'cross_encoder'
en2.model = CrossEncoder('sentence-transformers/ce-roberta-large-stsb')

en1 = NLP()
en1.lang = 'EN'
en1.type = 'siamese'
# en1.model = SentenceTransformer(models_path + 'sentBERT-allGood_wSingeltons')
en1.model = SentenceTransformer('distilroberta-base-paraphrase-v1')

fr1 = NLP()
fr1.lang = 'FR'
fr1.type = 'siamese'
fr1.model = SentenceTransformer('distilbert-multilingual-nli-stsb-quora-ranking')

fr2 = NLP()
fr2.lang = 'FR'
fr2.type = 'cross_encoder'
fr2.model = CrossEncoder('sentence-transformers/ce-roberta-large-stsb')

train_samples = [
  InputExample(texts=['aaa', 'bbb'], label=0.8),
  InputExample(texts=['aaa', 'ccc'], label=0.1),
]

to_train = CrossEncoder('sentence-transformers/ce-roberta-large-stsb', num_labels=1)

train_dataloader = DataLoader(train_samples, shuffle=True, batch_size=2)

to_train.fit(train_dataloader, epochs=5, warmup_steps=1, output_path=models_path + 'ceLarge_02')

to_train.save(models_path + 'ceLarge_02')

##########

test3 = CrossEncoder('mrm8488/camembert-base-finetuned-pawsx-fr', num_labels=2)

test3.fit(train_dataloader, epochs=5, warmup_steps=1, output_path=models_path + 'temp')

test3.save(models_path + 'test1')

test2 = CrossEncoder(models_path + 'test1')

test2.predict([['J\'ai faim','je veux maison']])
test2.predict([['J\'ai faim','j faime']])
test2.predict([['J\'ai faim','j\'ai faimm']])

#########

uqTxt=''
yesTxt = 'dog'
noTxt = 'music'
listString = ''

train_list = [
    ['reset','change',1.0],
    ['reset','forgot, unlock',0.0],
]
trainL(m, train_list)

uqTxt = 'locked out of anything'

yesTxt = 'locked out'
noTxt = 'home'
s3 = [uqTxt, yesTxt, noTxt]
comp2(m,s3)

# label order: Pos, Neg
train2(m,s3,[1.0, 0.0])
m.save(models_path + 'sentBERT-batch1')
ask1(m, uqTxt)

listString = ''
s2 = [uqTxt, listString]
compList(en2,s2)

print('\n')
listString = ''
s2 = [uqTxt, listString]
compList(m,s2)

#############

from sentence_transformers.cross_encoder import CrossEncoder
mCE = CrossEncoder('sentence-transformers/ce-roberta-base-stsb')

scores = mCE.predict([["I wanted to check on my request", "follow-up on a request; check on the status of a ticket or request"],  
                        ["I wanted to check on my request", "buying or ordering"]])
print(scores)


# sentence-transformers/ce-roberta-large-stsb
#     - best
y [0.5464668  0.24692366]
y [0.69994944 0.00888833]
n [0.4549483 0.5925973] - but entropy would trigger
y [0.48003206 0.00970908]

# sentence-transformers/ce-roberta-base-stsb
#   - worst from the EN ones, worse than roberta-base
[0.5761287 0.3306183]
[0.5888206  0.18477288]
[0.34705603 0.67005193]
[0.6892895  0.14602053]

# sentence-transformers/ce-distilroberta-base-stsb
    # - entropy might not trigger in the third
[0.46854043 0.36966506]
[0.57184106 0.12982018]
[0.39698684 0.6404734 ]
[0.4321674 0.0692763]

# sentence-transformers/distilbert-multilingual-nli-stsb-quora-ranking
#     - not working, plus it says "You should probably TRAIN this model on a down-stream task to be able to use it for predictions and inference"
[0.4961977 0.501594 ]
[0.49792215 0.50447816]
[0.514407  0.5140865]
[0.49828726 0.50957394]



