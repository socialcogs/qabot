from airtable import Airtable
import numpy as np
import pandas as pd
import math
import time

airtConn2Main = Airtable(...)
airtConn2CoQs = Airtable(...)

# >>> To Do: 
#        maxRecords removal or attention to =100 limit

def load_dfM():
    airtMain = airtConn2Main.get_all()
    # DataFrame of Main table
    df1 = pd.DataFrame.from_dict(airtMain)
    df1 = df1.iloc[:,0]
    df2 = pd.DataFrame.from_dict((r['fields'] for r in airtMain))
    dfM = pd.concat([df1, df2],axis=1)
    dfM['AlgTxt']= dfM['AlgTxt'].astype('string')
    dfM['Txt2User']= dfM['Txt2User'].astype('string')
    dfM['id']= dfM['id'].astype('string')
    dfM['NER Jump']= dfM['NER Jump'].astype('string')
    
    return dfM


def load_dfCq():
    airtCoQs = airtConn2CoQs.get_all()
    # DataFrame of ContextExpQ table
    df1 = pd.DataFrame.from_dict(airtCoQs)
    df1 = df1.iloc[:,0]
    df2 = pd.DataFrame.from_dict((r['fields'] for r in airtCoQs))
    dfCq = pd.concat([df1, df2],axis=1)
    
    dfCq['Skip If This RegEx Matches']= \
    dfCq['Skip If This RegEx Matches'].astype('string')
    
    dfCq['#1 Addition to Context']= dfCq['#1 Addition to Context'].astype('string')
    dfCq['#2 Prompt To User']= dfCq['#2 Prompt To User'].astype('string')
    dfCq['#1 Prompt To User']= dfCq['#1 Prompt To User'].astype('string')
    dfCq['#2 Addition to Context']= dfCq['#2 Addition to Context'].astype('string')
    dfCq['Txt']= dfCq['Txt'].astype('string')
    
    return dfCq


def updateAirtable(nID, field, value):
    print(nID, field, value)
    updFields = {}
    updFields[field] = value
    airtConn2Main.update(nID, updFields)
    time.sleep(0.2)
        

def auto_complete_links(dfM):
    # both dataframes & write back to Airtable
    
    logTxt = 'Move prints in load_and_fix data TO logHandle'
    # Complete parent-child bidirectionality
    for index, row in dfM.iterrows():
        # Auto-fill Parents field for all Children of this row
        children = row['Children']
        thisParent = row['id']
        if type(children) is list:
            for chID in children:
                chDfI = dfM[dfM.id==chID].index[0]
                # if parents is a list, make sure [thisParent] in it
                if type(dfM.at[chDfI, 'Parents']) is list:
                    if thisParent not in dfM.at[chDfI, 'Parents']:
                        dfM.at[chDfI, 'Parents'].append(thisParent)
                        updateAirtable(chID, 'Parents', \
                                      dfM.at[chDfI, 'Parents'])
                elif math.isnan(dfM.at[chDfI, 'Parents']):
                    dfM.at[chDfI, 'Parents'] = [thisParent]
                    updateAirtable(chID, 'Parents', \
                                      dfM.at[chDfI, 'Parents'])
                else:
                    print('* ERROR')
                    # else error >>>>>>>>>> TO IMPLEMENT
        # Auto-fill Children for all the Parents
        parents = row['Parents']
        thisChild = row['id']
        # on all parents: their Children must include this thisChild
        if type(parents) is list:
            for parID in parents:
                parDfI = dfM[dfM.id==parID].index[0]
                # if Children is a list, make sure [thisChild] in it
                if type(dfM.at[parDfI, 'Children']) is list:
                    if thisChild not in dfM.at[parDfI, 'Children']:
                        dfM.at[parDfI, 'Children'].append(thisChild)
                        updateAirtable(parID, 'Children', \
                                      dfM.at[parDfI, 'Children'])
                elif math.isnan(dfM.at[parDfI, 'Children']):
                    dfM.at[parDfI, 'Children'] = [thisChild]
                    updateAirtable(parID, 'Children', \
                                      dfM.at[parDfI, 'Children'])
                else:
                    print('* ERROR')
                    # else error >>>>>>>>>> TO IMPLEMENT 
        
    return logTxt
