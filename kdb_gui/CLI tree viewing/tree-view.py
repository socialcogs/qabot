import pandas as pd
import math
from airtable import Airtable
from pprint import pprint
import os
from treelib import Node, Tree
import time

airtConn2Main = Airtable(...)

airtMain = airtConn2Main.get_all()

# DataFrame of Main table
df1 = pd.DataFrame.from_dict(airtMain)
df1 = df1.iloc[:,0]
df2 = pd.DataFrame.from_dict((r['fields'] for r in airtMain))
dfM = pd.concat([df1, df2],axis=1)
dfM['AlgTxt']= dfM['AlgTxt'].astype('string')
dfM['id']= dfM['id'].astype('string')
    
tM = Tree()
tInx = 1
tM.create_node("root", tInx)
idDict = {}

# parse starting from the <root>
def parse_airtable_into_tree(ID):
    global tInx
    parentInx = tInx
    #print('parsing: ', ID)
    children = dfM[dfM.id==ID]['Children'].iloc[0]
    #print(children)
    if type(children) is list:
        for chID in children:
            nodeTxt = dfM[dfM.id==chID]['AlgTxt'].iloc[0][:50]
            tInx = tInx + 1
            nodeTxt = str(tInx) + ': ' + nodeTxt
            tM.create_node(nodeTxt, tInx, \
                            parent = parentInx)
            parse_airtable_into_tree(chID)

defaultID = dfM[dfM.AlgTxt=='<Default>']['id'].iloc[0]
parse_airtable_into_tree(defaultID)
tM.show()