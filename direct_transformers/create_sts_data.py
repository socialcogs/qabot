import pickle
import pandas as pd

dfM = pickle.load(open("kdb.pickle","rb"))

train_data = []

# parse starting from the <root>
def parse_tree(ID, prev_pathSeq):
    if ID != 'ROOT': new_pathSeq = prev_pathSeq + [ID]
    else: new_pathSeq = prev_pathSeq
    children = dfM[dfM.id==ID]['Children'].iloc[0]
    if type(children) is list:
        # # Non-similarities between all children
        # all_pairs = [(a, b) for idx, a in enumerate(children) for b in children[idx + 1:]] 
        # for (x,y) in all_pairs:
        #     x_AlgTxt = dfM[dfM.id == x]['EN_AlgTxt'].iloc[0]
        #     y_AlgTxt = dfM[dfM.id == y]['EN_AlgTxt'].iloc[0]
        #     x_subs = x_AlgTxt.split(';')
        #     y_subs = y_AlgTxt.split(';')
        #     for xS in x_subs:
        #         for yS in y_subs:
        #             train_data.append([xS, yS, 0.0])
        # Similarities with all previous path nodes
        for chID in children:
            myAlgTxt = dfM[dfM.id == chID]['EN_AlgTxt'].iloc[0]
            for prevNode in prev_pathSeq:
                nodeAlgTxt = dfM[dfM.id == prevNode]['EN_AlgTxt'].iloc[0]
                my_subs = myAlgTxt.split(';')
                node_subs = nodeAlgTxt.split(';')
                for myS in my_subs:
                    for nodeS in node_subs:
                        train_data.append([myS, nodeS, 5.0])
            parse_tree(chID, new_pathSeq)

parse_tree('ROOT',[])

train_df = pd.DataFrame(train_data)
train_df.columns = ["text_a", "text_b", "labels"]
pickle.dump(train_df, open("train_df.pickle", "wb"))

eval_data = [
    ['aaa','bbb',5.0],
    ['aaa','ccc',0.0]
]
eval_df = pd.DataFrame(eval_data)
eval_df.columns = ["text_a", "text_b", "labels"]
pickle.dump(eval_df, open("eval_df.pickle", "wb"))