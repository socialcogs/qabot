import torch

from transformers import AutoTokenizer, AutoModelForSequenceClassification
tokenizer = AutoTokenizer.from_pretrained("textattack/roberta-base-STS-B")
model = AutoModelForSequenceClassification.from_pretrained("textattack/roberta-base-STS-B", num_labels=1)

userQ = "I am angry"
opt1 = "I would like to drink water"

outVec = tokenizer(userQ, opt1, return_tensors="pt")
classification_logits = model(**outVec).logits
classification_logits




