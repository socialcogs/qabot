from simpletransformers.classification import ClassificationModel

import pandas as pd
import logging
import pickle

logging.basicConfig(level=logging.INFO)
transformers_logger = logging.getLogger("transformers")
transformers_logger.setLevel(logging.WARNING)

# Preparing train data
train_data = [
    [
        "aaa",
        "bbb",
        5.0,
    ],
    [
        "aaa",
        "ccc",
        0.0,
    ],
]
train_df = pd.DataFrame(train_data)
train_df.columns = ["text_a", "text_b", "labels"]

# # Preparing eval data
# eval_data = [
...
# ]
# eval_df = pd.DataFrame(eval_data)
# eval_df.columns = ["text_a", "text_b", "labels"]

# train_df = pickle.load(open("train_df.pickle","rb"))
# eval_df = pickle.load(open("eval_df.pickle","rb"))

# Optional model configuration
#model_args = ClassificationArgs(num_train_epochs=1)
train_args = {
    'reprocess_input_data': True,
    'overwrite_output_dir': True,
    # 'evaluate_during_training': True,
    # 'max_seq_length': 512,
    'num_train_epochs': 5,
    'num_labels' : 1,
    # 'evaluate_during_training_steps': 5,
    # 'wandb_project': 'sts-b-medium',
    # 'train_batch_size': 5,
    'regression': True,
}

# Create a ClassificationModel
### Remember CUDA switch
model = ClassificationModel("roberta", "textattack/roberta-base-STS-B", args=train_args, use_cuda=False)

# Train the model
model.train_model(train_df)

# Evaluate the model
result, model_outputs, wrong_predictions = model.eval_model(eval_df)

print(model.predict([['aaa','bbb']]))

##############
# model2 = ClassificationModel("roberta", "outputs/", num_labels=1, use_cuda=False)



