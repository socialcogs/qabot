import pickle
import numpy as np
import pandas as pd
import scipy.spatial
from sentence_transformers import SentenceTransformer, InputExample, losses
from torch.utils.data import DataLoader

models_path = /home/martindkaz/Dropbox/NLP_resources/nlp_models/
output_path=models_path + 'tempModel'

def comp2(model,s3):
    uqTxt = s3[0]
    yesTxt = s3[1]
    noTxt = s3[2]
    print("uqTxt = \'" + uqTxt + "\'")
    uqEmb = nlp.model.encode([uqTxt])
    # print(uqEmb[0][0])

    sEmb = nlp.model.encode([yesTxt])
    cosDist = scipy.spatial.distance.cdist(uqEmb, sEmb, "cosine")[0]
    print("yesTxt: ", yesTxt, cosDist)
    # print(sEmb)

    sEmb = nlp.model.encode([noTxt])
    cosDist = scipy.spatial.distance.cdist(uqEmb, sEmb, "cosine")[0]
    print("noTxt: ", noTxt, cosDist)
    # print(sEmb)

def trainL(model, train_list):
    train_examples = []
    for di in train_list:
        train_examples = train_examples + [InputExample(texts=[di[0],di[1]], label=float(di[2]))]

    train_dataloader = DataLoader(train_examples, shuffle=True, batch_size=len(train_list))
    train_loss = losses.CosineSimilarityLoss(model)

    eval_data = pickle.load(open("pickle.eval_list","rb"))
    sentences1 = []
    sentences2 = []
    scores = []
    for di in eval_data:
        sentences1 = sentences1 + [di[0]]
        sentences2 = sentences2 + [di[1]]
        scores = scores + [float(di[2])]

    from sentence_transformers import evaluation
    evaluator = evaluation.EmbeddingSimilarityEvaluator(sentences1, sentences2, scores)

    nlp.model.fit(train_objectives=[(train_dataloader, train_loss)], epochs=10, warmup_steps=1, evaluator=evaluator, evaluation_steps=1, output_path=output_path)

def train2(model,s3,labels):
    pos = labels[0]
    neg = labels[1]
    uqTxt = s3[0]
    yesTxt = s3[1]
    noTxt = s3[2]
    print("Before training:")
    comp2(model,s3)

    # train_data = pickle.load(open("pickle.train_list","rb"))
    train_data = [
        [uqTxt, yesTxt, pos],
        [uqTxt, noTxt, neg],
    ]

    train_examples = []
    for di in train_data:
        train_examples = train_examples + [InputExample(texts=[di[0],di[1]], label=float(di[2]))]

    train_dataloader = DataLoader(train_examples, shuffle=True, batch_size=16)
    train_loss = losses.CosineSimilarityLoss(model)

    eval_data = pickle.load(open("pickle.eval_list","rb"))
    sentences1 = []
    sentences2 = []
    scores = []
    for di in eval_data:
        sentences1 = sentences1 + [di[0]]
        sentences2 = sentences2 + [di[1]]
        scores = scores + [float(di[2])]

    from sentence_transformers import evaluation
    evaluator = evaluation.EmbeddingSimilarityEvaluator(sentences1, sentences2, scores)

    nlp.model.fit(train_objectives=[(train_dataloader, train_loss)], epochs=10, warmup_steps=1, evaluator=evaluator, evaluation_steps=1, output_path=output_path)

    print("After training:")
    comp2(model,s3)

def compList(nlp,s2):
    uqTxt = s2[0]
    listString = s2[1]
    print("uqTxt = ", uqTxt)
    if nlp.type == 'siamese':
        uqEmb = nlp.model.encode([uqTxt])
        txtList = listString.split(';')
        for sTxt in txtList:
            sEmb = nlp.model.encode([sTxt])
            cosDist = scipy.spatial.distance.cdist(uqEmb, sEmb, "cosine")[0]
            print(sTxt, cosDist)
    elif nlp.type == 'cross_encoder':
        txtList = listString.split(';')
        for sTxt in txtList:
            score = nlp.model.predict([[uqTxt, sTxt]])
            print(score)